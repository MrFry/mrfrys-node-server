const fs = require('fs') // eslint-disable-line
const dir = '../stats'

// ------------------------------------------------------------------------------------
// Params
// ------------------------------------------------------------------------------------

const pathsToAddAsCol = ['isAdding', 'ask', 'registerscript', 'hasNewMsg']

// ------------------------------------------------------------------------------------
// Daily stats
// ------------------------------------------------------------------------------------

const result = []
const dailyDataCount = readFile(`${dir}/dailyDataCount`)

dailyDataCount.split('\n').forEach((line) => {
  try {
    const data = JSON.parse(line)
    const date = new Date(data.date)
    result.push({ ...data, date: date })
  } catch (e) {
    //
  }
})

// ------------------------------------------------------------------------------------
// User ID Requests
// ------------------------------------------------------------------------------------

const asd = 5
const userIdRequests = readJSON(`${dir}/uvstats`)
Object.keys(userIdRequests).forEach((key) => {
  const date = new Date(key)
  const ri = result.findIndex((res) => {
    return dateEquals(res.date, date)
  })
  if (ri !== -1) {
    result[ri] = {
      ...result[ri],
      uniqueUserIdRequests: Object.keys(userIdRequests[key]).length,
      [`uniqueUserIdRequestsMin${asd}`]: Object.keys(
        userIdRequests[key]
      ).reduce((acc, subKey) => {
        if (userIdRequests[key][subKey] > asd) {
          acc += 1
        }
        return acc
      }, 0),
    }
  } else {
    result.push({
      date: date,
      [`uniqueUserIdRequestsMin${asd}`]: Object.keys(
        userIdRequests[key]
      ).reduce((acc, subKey) => {
        if (userIdRequests[key][subKey] > asd) {
          acc += 1
        }
        return acc
      }, 0),
    })
  }
})

// ------------------------------------------------------------------------------------
// User ID Test solving
// ------------------------------------------------------------------------------------

const userIdTestSolving = readJSON(`${dir}/idvstats`)
Object.keys(userIdTestSolving).forEach((key) => {
  const date = new Date(key)
  const ri = result.findIndex((res) => {
    return dateEquals(res.date, date)
  })
  if (ri !== -1) {
    result[ri] = {
      ...result[ri],
      uniqueUserIdTestSolving: Object.keys(userIdTestSolving[key]).length,
    }
  } else {
    result.push({
      date: date,
      uniqueUserIdTestSolving: Object.keys(userIdTestSolving[key]).length,
    })
  }
})

// ------------------------------------------------------------------------------------
// Website acesses
// ------------------------------------------------------------------------------------

const stats = readJSON(`${dir}/vstats`)
Object.keys(stats).forEach((key) => {
  const date = new Date(key)
  const ri = result.findIndex((res) => {
    return dateEquals(res.date, date)
  })

  const toAdd = pathsToAddAsCol.reduce((acc, path) => {
    return {
      ...acc,
      [`Path: ${path}`]: stats[key][getKeyName(path, stats[key])],
    }
  }, {})

  if (ri !== -1) {
    result[ri] = {
      ...result[ri],
      ...toAdd,
    }
  } else {
    result.push({
      date: date,
      ...toAdd,
    })
  }
})

function getKeyName(keyName, obj) {
  return Object.keys(obj).find((key) => {
    return key.includes(keyName)
  })
}

// ------------------------------------------------------------------------------------
// Registered script count
// ------------------------------------------------------------------------------------

const registeredScripts = readJSON(`${dir}/registeredScripts.json`)
let total = 0
const registeredScriptsByDay = registeredScripts
  .reduce((acc, item) => {
    const date = new Date(item.date)
    const ri = acc.findIndex((res) => {
      return dateEquals(res.date, date)
    })
    if (ri !== -1) {
      acc[ri] = {
        ...acc[ri],
        registeredScripts: acc[ri].registeredScripts + 1,
      }
    } else {
      acc.push({
        date: date,
        registeredScripts: 1,
      })
    }
    return acc
  }, [])
  .sort((a, b) => {
    return a.date.getTime() - b.date.getTime()
  })
  .map((item) => {
    total += item.registeredScripts
    return {
      ...item,
      registeredScripts: total,
    }
  })

registeredScriptsByDay.forEach((item) => {
  const date = item.date
  const ri = result.findIndex((res) => {
    return dateEquals(res.date, date)
  })

  if (ri !== -1) {
    result[ri] = {
      ...result[ri],
      registeredScripts: item.registeredScripts,
    }
  } else {
    result.push({
      date: date,
      registeredScripts: item.registeredScripts,
    })
  }
})

// process.exit()

// ------------------------------------------------------------------------------------
// Exporting
// ------------------------------------------------------------------------------------

const csv = arrayToCsv(
  result.sort((a, b) => {
    return a.date.getTime() - b.date.getTime()
  })
)

fs.writeFileSync('./res.csv', csv)

// ------------------------------------------------------------------------------------

function arrayToCsv(array) {
  const columns = array.reduce((acc, item) => {
    const keys = Object.keys(item)
    keys.forEach((key) => {
      if (!acc.includes(key)) {
        acc.push(key)
      }
    })

    return acc
  }, [])

  const res = [columns.join(';')]
  array.forEach((item) => {
    res.push(
      columns
        .map((key) => {
          if (key !== 'date') {
            return item[key] ? item[key] : ''
          } else {
            return item[key] ? item[key].toDateString() : ''
          }
        })
        .join(';')
    )
  })

  return res.join('\n')
}

function readFile(name) {
  if (fs.existsSync(name)) {
    return fs.readFileSync(name, 'utf8')
  }
}

function readJSON(name) {
  return JSON.parse(fs.readFileSync(name, 'utf8'))
}

function dateEquals(d1, d2) {
  return (
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate() &&
    d1.getFullYear() === d2.getFullYear()
  )
}
