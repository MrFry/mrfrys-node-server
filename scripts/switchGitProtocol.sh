#!/bin/sh

gitDir=".git"
gitmoduleFile=".gitmodules"
sshGitmodulesFile=".gitmodules.ssh"
httpsGitmodulesFile=".gitmodules.https"

error() { printf "\033[0;41m%s\033[0m\n" "${1}"; exit 1; }

if [ ! -d "${gitDir}" ]; then
    error "${gitDir} does not exist! Maybe you ran this script in the wrong directory"
fi

if [ "$1" != "ssh" ] && [ "$1" != "https" ]; then
    error "Invalid param! Valid params are: 'ssh', 'https'"
fi

if [ "$1" = "ssh" ]; then
    cp -v "$sshGitmodulesFile" "$gitmoduleFile"
elif [ "$1" = "https" ]; then
    cp -v "$httpsGitmodulesFile" "$gitmoduleFile"
fi
