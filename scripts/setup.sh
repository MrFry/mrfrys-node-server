#!/bin/sh

baseDir=$(pwd)
domainPath="./data/domain"

hr() { [ -t 0 ] && printf '\033[0;32m%*s\033[0m\n' "$(tput cols)" '' | tr ' ' '='; }

log() { hr; printf "\033[0;34m%s\033[0m\n" "${1}"; hr; }

error() { printf "\033[0;41m%s\033[0m\n" "${1}"; }

checkFile() {
    if [ ! -f "${1}" ]; then
        error "${1} does not exist!"
        exit 1
    fi
}

makeNextSubmodule() {
    log "Making ${1}"
    cd "submodules/${1}/" || exit 1
    npm install
    npm audit fix > /dev/null 2> /dev/null
    npm run export || exit 1
    npx --yes next telemetry disable
    cd "${baseDir}" || exit 1
    linkTarget="$PWD/nextStatic/${2}"
    if [ ! -e "${linkTarget}" ]; then
        ln -sfv "$PWD/submodules/${1}/out" "${linkTarget}"
    fi
}

# ------------------------------------------------------------------------------------
# Checking if we are ready to go
# ------------------------------------------------------------------------------------
checkFile "$PWD/src/server.ts"
checkFile "$PWD/package.json"
checkFile "$PWD/package-lock.json"

domain="${DOMAIN}"

if [ -z "${domain}" ] && [ -f "${domainPath}" ]; then
    domain=$(cat "${domainPath}")
fi

if [ -z "${domain}" ]; then
    error "DOMAIN is not set, and ${domainPath} does not exist!"
    echo "Set DOMAIN to the preferred domain"
    echo "Or please create the file ${domainPath}, and re-run this script"
    echo "Expected content: domain, ex.:'qmining.com' (without 'http://' and '/')"
    exit 1
fi

# ------------------------------------------------------------------------------------
# Creating directory structure
# ------------------------------------------------------------------------------------
log "mkdir-ing"
# TODO: make server create these itself
mkdir -pv stats
mkdir -pv stats/logs
mkdir -pv stats/vlogs
mkdir -pv data
mkdir -pv data/dbs
mkdir -pv public

touch public/motd

cp -vrn ./defaultPublicFiles/* ./public/

# ------------------------------------------------------------------------------------
# Git submodules
# ------------------------------------------------------------------------------------
if [ ! -z "$CLONE_WITH_HTTPS" ]; then
    ./scripts/switchGitProtocol.sh "https"
fi

log 'Updating / Cloning project'
git pull || exit 1
git submodule update --init --recursive --remote || exit 1

# ------------------------------------------------------------------------------------
# DB-s
# ------------------------------------------------------------------------------------
log "Making project"
npm install
npm audit fix > /dev/null 2> /dev/null
npm run export

# ------------------------------------------------------------------------------------
# Next js submodules
# ------------------------------------------------------------------------------------
mkdir "$PWD/nextStatic"
makeNextSubmodule "qmining-page" "qminingPagePublic"
makeNextSubmodule "qmining-data-editor" "dataEditorPublic"

# ------------------------------------------------------------------------------------
# Moodle test userscript
# ------------------------------------------------------------------------------------
log "Making moodle test userscript"
checkFile "$PWD/submodules/moodle-test-userscript/stable.user.js"
mkdir -pv "$PWD/public/moodle-test-userscript"
ln -sfv "$PWD/submodules/moodle-test-userscript/stable.user.js" "$PWD/public/moodle-test-userscript/"

# ------------------------------------------------------------------------------------
# DB-s
# ------------------------------------------------------------------------------------
if [ -z "$(ls -A ./data/dbs)" ]; then
    log "Making DB-s"
    cd src/standaloneUtils || exit 1
    NS_SQL_DEBUG_LOG=1 NS_LOGLEVEL=2 node dbSetup.js
    mv ./*.db ../../data/dbs
    cd "${baseDir}" || exit 1
else
    log "Dbs already exist, skipping"
fi

# ------------------------------------------------------------------------------------
log "Done! development mode: 'npm run dev', prod mode: 'npm run start', tests: 'npm run test'"
exit 0
