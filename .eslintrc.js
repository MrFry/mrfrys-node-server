module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    jest: true,
  },
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  rules: {
    'no-undef': ['error'],
    eqeqeq: ['warn', 'smart'],
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/explicit-module-boundary-types': [
      'warn',
      { allowArgumentsExplicitlyTypedAsAny: true },
    ],
    'no-prototype-builtins': 'off',
    '@typescript-eslint/ban-types': 'off',
    'id-length': [
      'warn',
      { exceptions: ['i', 'j', 't', 'Q', 'A', 'C', 'q', 'a', 'b', 'x', 'e'] },
    ],
    'object-shorthand': ['warn', 'never'],
    'prefer-const': 'warn',
  },
  root: true,
}
