#!/bin/bash
url=$(head -n 1 ../serverAddress)
echo "Server url: $url"

simpleType='\{"type":"simple"\}'
q=$1
data=$2
subj=$3
if [ "$#" -lt 2 ]; then
  echo "min 2 params required"
  echo "Params can be:"
  echo "question data subj"
  echo "question subj"
  exit 1
fi

if [ "$#" -eq 2 ]; then
  data="$simpleType"
  subj=$2
elif [ "$#" -eq 2 ]; then
  data=$2
  subj=$3
fi

../bin/hr.sh

data=$(node -e "console.log(encodeURIComponent('$data'));")

echo sending
echo "Question: $q"
echo "Data: $data"
echo "Subject: $subj"

q=$(node -e "console.log(encodeURIComponent('$q'))")
if [ "$?" -ne 0 ]; then
  echo "node error!"
  exit 1
fi

subj=$(node -e "console.log(encodeURIComponent('$subj'))")
if [ "$?" -ne 0 ]; then
  echo "node error!"
  exit 1
fi

echo "Result:"
../bin/hr.sh
res=$(curl --cookie "sessionID=e0ac328d-86cc-4dbf-a00b-213bec6011e7" -H "Content-Type: application/json" -L -s -X GET "${url}/ask?q=${q}&data=${data}&subj=${subj}")
echo "$res" | jq
if [ "$?" -ne 0 ]; then
  echo "jq error"
  echo "$res"
fi

../bin/hr.sh
