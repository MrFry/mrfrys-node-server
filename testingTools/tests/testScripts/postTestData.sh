#!/bin/bash
url=$(head -n 1 ../serverAddress)
echo "Server url: $url"

if [ "$#" -ne 1 ]; then
  echo "1 param required: json to send to localhost, got $#"
else
  data=$(tr -d '\n' <"$1" | awk '$1=$1')
  # data=$(node -e "console.log(encodeURIComponent('$data'));")
  echo sending
  echo "SENT DATA:"
  ../bin/hr.sh
  echo "$data"
  ../bin/hr.sh
  curl \
     --cookie "sessionID=e0ac328d-86cc-4dbf-a00b-213bec6011e7" \
    -H "Content-Type: application/json" \
    -X POST --data "$data" \
    "$url/api/isAdding"
fi
