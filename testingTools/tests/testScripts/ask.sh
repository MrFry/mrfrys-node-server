#!/bin/bash
url=$(head -n 1 ../serverAddress)
echo "Server url: $url"

simpleType="{\"type\":\"simple\"}"
defaultTestUrl='https://elearning.uni-obuda.hu/'
sessid='e0ac328d-86cc-4dbf-a00b-213bec6011e7'

q=$1
subj=$2
testUrl=$3
data=$4

if [ "$#" -lt 2 ]; then
  echo "min 2 params required"
  echo "Params can be:"
  echo "question subj testUrl"
  echo "question subj testUrl data"
  exit 1
fi

if [ "$#" -eq 2 ]; then
  data="$simpleType"
  testUrl="$defaultTestUrl"
elif [ "$#" -eq 3 ]; then
  data="$simpleType"
fi

../bin/hr.sh

echo "Question: $q"
echo "Subject: $subj"
echo "testUrl: $testUrl"
echo "Data: $data"
../bin/hr.sh
echo "sent data":
sentdata="{
  \"questions\": [
      {
        \"Q\": \"${q}\",
        \"subj\": \"${subj}\",
        \"data\": ${data}
      }
    ],
  \"testUrl\": \"${testUrl}\"
  }"
echo "$sentdata" | jq

if [ "$?" -ne 0 ]; then
  echo "jq error, aborting sending"
  echo "$sentdata"
  exit 1
fi

../bin/hr.sh
echo "Result:"
res=$(curl --cookie "sessionID=${sessid}" \
  -H "Content-Type: application/json" -L -s -X POST \
  -d "${sentdata}" \
  "${url}/ask")

echo "$res" | jq

if [ "$?" -ne 0 ]; then
  echo "jq error"
  echo "$res"
fi

echo "done"
../bin/hr.sh
