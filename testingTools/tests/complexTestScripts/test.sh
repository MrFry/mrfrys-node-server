#!/bin/bash

echo "Adding simple questions, and asking from some of them"
echo "TEST EXPECTED BEHAVIOUR:"
echo "Second question should have result too"

../bin/hr.sh

../testScripts/postTestData.sh "../testData/igazHamis.json"

../testScripts/ask.sh "Ha a vállalati saját tőkéje 25M, és az összes kötelezettsége 100M, mekkora a vállal at tőkeellátottsági mtatója" "Válgazd II. 2020S BSc Nap EA"

../testScripts/ask.sh "A készletezési periódus a szállítói számla kiegyenlítésétől a vevővel szembeni köve telés beérkezéséig tart." "Válgazd II. 2020S BSc Nap EA"
