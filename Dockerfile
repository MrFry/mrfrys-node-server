FROM docker.io/node:19.8.1-alpine3.17 as builder

WORKDIR /server

RUN apk add python3 \
            make \
            gcc \
            g++ \
            libc-dev \
            git \
            openssh-client \
            bash

COPY . .
RUN npm install
RUN bash ./scripts/setup.sh

FROM docker.io/node:19.8.1-alpine3.17

# required for some API info
RUN apk add git

WORKDIR /server
COPY --from=builder /server .

CMD ["npm", "run", "start"]
