import { paths } from './utils/files'
import utils from './utils/utils'

const domain = process.env.DOMAIN || utils.ReadFile(paths.domainFile).trim()

if (!domain) {
    throw new Error(
        `Domain is undefined! Should be set with 'DOMAIN' environment variable, or written to '${paths.domainFile}'`
    )
}

export default {
    savedQuestionsFileName: 'savedQuestions.json',
    domain: domain,

    // --------------------------------------------------------------------------------
    // user files
    // --------------------------------------------------------------------------------
    userFilesDataFileName: '.data.json',
}
