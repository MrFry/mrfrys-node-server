import { parentPort } from 'node:worker_threads'
import { cleanDb } from '../../utils/qdbUtils'
import { Question, QuestionDb } from '../../types/basicTypes'
import { WorkerResult } from '../worker'

export type DbCleanTaskObject = {
    type: 'dbClean'
    data: {
        questions: Question[]
        subjToClean: string
        overwriteBeforeDate: number
        qdbIndex: number
    }
}

export const handleDbClean = async (
    qdbs: QuestionDb[],
    msg: DbCleanTaskObject,
    workerIndex: number
): Promise<void> => {
    const removedIndexes = cleanDb(msg.data, qdbs)

    const workerResult: WorkerResult = {
        msg: `From thread #${workerIndex}: db clean done`,
        workerIndex: workerIndex,
        result: removedIndexes,
    }

    parentPort.postMessage(workerResult)
}
