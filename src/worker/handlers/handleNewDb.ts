import { parentPort } from 'node:worker_threads'
import { QuestionDb } from '../../types/basicTypes'

export type NewDbTaskObject = {
    type: 'newdb'
    data: QuestionDb
}

export const handleNewDb = async (
    qdbs: QuestionDb[],
    msg: NewDbTaskObject,
    workerIndex: number,
    setQdbs: (newVal: Array<QuestionDb>) => void
): Promise<void> => {
    const { data }: { data: QuestionDb } = msg
    setQdbs([...qdbs, data])

    parentPort.postMessage({
        msg: `From thread #${workerIndex}: new db add done`,
        workerIndex: workerIndex,
    })
}
