/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

const DbStruct = {
    users: {
        tableStruct: {
            id: {
                type: 'integer',
                primary: true,
                autoIncrement: true,
            },
            pw: {
                type: 'text',
                notNull: true,
                unique: true,
            },
            notes: {
                type: 'text',
            },
            loginCount: {
                type: 'number',
                defaultZero: true,
            },
            created: {
                type: 'number',
                notNull: true,
            },
            lastLogin: {
                type: 'number',
            },
            lastAccess: {
                type: 'number',
            },
            avaiblePWRequests: {
                type: 'number',
                defaultZero: true,
            },
            pwRequestCount: {
                type: 'number',
                defaultZero: true,
            },
            createdBy: {
                type: 'number',
            },
            sourceHost: {
                type: 'text',
            },
            isAdmin: {
                type: 'integer',
            },
        },
    },
    sessions: {
        foreignKey: [
            {
                keysFrom: ['userID'],
                table: 'users',
                keysTo: ['id'],
            },
        ],
        tableStruct: {
            id: {
                type: 'text',
                primary: true,
                notNull: true,
            },
            userID: {
                type: 'number',
                notNull: true,
            },
            createDate: {
                type: 'number',
                notNull: true,
            },
            lastAccess: {
                type: 'number',
            },
            isScript: {
                type: 'number',
                notNull: true,
            },
        },
    },
}

exports.default = DbStruct
