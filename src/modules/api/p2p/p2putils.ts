import { PeerInfo, QuestionDb } from '../../../types/basicTypes'
import { files, paths, readAndValidateFile } from '../../../utils/files'
import logger from '../../../utils/logger'
import {
    PostResult,
    downloadFile,
    parseCookie,
    post,
} from '../../../utils/networkUtils'
import utils from '../../../utils/utils'
import { UserDirDataFile } from '../submodules/userFiles'

export interface SyncResponseBase {
    success: boolean
    message?: string
}

export interface RemotePeerInfo {
    selfInfo: PeerInfo
    myPeers: PeerInfo[]
    serverRevision?: string
    scriptRevision?: string
    qminingPageRevision?: string
    dataEditorRevision?: string
    serverLastCommitDate?: number
    scriptLastCommitDate?: number
    qminingPageLastCommitDate?: number
    dataEditorLastCommitDate?: number
    serverBuildTime?: number
    qminingPageBuildTime?: number
    dataEditorBuildTime?: number
    scriptVersion?: string
    userCount?: number
    qdbInfo?: {
        questionDbCount: number
        subjectCount: number
        questionCount: number
    }
}

export interface SyncResult {
    old?: { [key: string]: number }
    added?: { [key: string]: number }
    final?: { [key: string]: number }
    msg?: string
}

export interface SyncDataResult {
    remoteInfo?: RemotePeerInfo
    users?: SyncResponseBase & {
        encryptedUsers?: string
        sentUsers?: number
    }
    questions?: SyncResponseBase & {
        questionDbs: QuestionDb[]
        count: {
            qdbs: number
            subjects: number
            questions: number
        }
    }
    userFiles?: SyncResponseBase & {
        newFiles: {
            [key: string]: {
                [key: string]: UserDirDataFile
            }
        }
    }
}

export function peerToString(peer: {
    host: string
    port: string | number
}): string {
    return `${peer.host}:${peer.port}`
}

export function isPeerSameAs(
    peer1: { host: string; port: number },
    peer2: { host: string; port: number }
): boolean {
    return peer1.host === peer2.host && peer1.port === peer2.port
}

export function updateLastSync(selfInfo: PeerInfo, newDate: number): void {
    utils.WriteFile(
        JSON.stringify({ ...selfInfo, lastSync: newDate }, null, 2),
        paths.selfInfoFile
    )
}

export function updateThirdPartyPeers(
    newVal: Omit<PeerInfo, 'publicKey' | 'name' | 'contact'>[]
): void {
    const prevVal = utils.FileExists(paths.thirdPartyPeersFile)
        ? utils.ReadJSON<PeerInfo[]>(paths.thirdPartyPeersFile)
        : []

    const dataToWrite = newVal.reduce((acc, peer) => {
        const isIncluded = acc.find((x) => {
            return peerToString(x) === peerToString(peer)
        })
        if (!isIncluded) {
            return [...acc, peer]
        }
        return acc
    }, prevVal)

    utils.WriteFile(
        JSON.stringify(dataToWrite, null, 2),
        paths.thirdPartyPeersFile
    )
}

export function updatePeersFile(
    peerToUpdate: PeerInfo,
    updatedPeer: Partial<PeerInfo>
): void {
    const newVal = readAndValidateFile<PeerInfo[]>(files.peersFile)
    let peers
    if (newVal) {
        peers = newVal
    }
    if (!peers)
        throw new Error('Peers file was invalid while trying to update it!')

    const updatedPeers = peers.map((x) => {
        if (isPeerSameAs(peerToUpdate, x)) {
            return {
                ...x,
                ...updatedPeer,
            }
        } else {
            return x
        }
    })

    utils.WriteFile(JSON.stringify(updatedPeers, null, 2), paths.peersFile)
}

export async function loginToPeer(peer: PeerInfo): Promise<string | Error> {
    const { data, error, cookie } = await post<{
        result: string
        msg: string
    }>({
        hostname: peer.host,
        path: '/api/login',
        port: peer.port,
        bodyObject: { pw: peer.pw },
        http: peer.http,
    })

    if (error || !data || data.result !== 'success') {
        return data ? new Error(data.msg) : error
    }

    const parsedCookies = parseCookie(cookie)
    return parsedCookies.sessionID
}

export async function loginAndPostDataToAllPeers<
    T extends { result?: string; success?: boolean }
>(
    peers: PeerInfo[],
    postDataFn: (
        peer: PeerInfo,
        sessionCookie: string
    ) => Promise<PostResult<T>>,
    resultCallback?: (peer: PeerInfo, res: PostResult<T>) => void
): Promise<void> {
    const results: {
        errors: PeerInfo[]
        sent: PeerInfo[]
        loginErrors: PeerInfo[]
    } = {
        errors: [],
        sent: [],
        loginErrors: [],
    }

    for (const peer of peers) {
        try {
            let sessionCookie = peer.sessionCookie

            const login = async (peer: PeerInfo) => {
                const loginResult = await loginToPeer(peer)
                if (typeof loginResult === 'string') {
                    sessionCookie = loginResult
                    updatePeersFile(peer, { sessionCookie: loginResult })
                } else {
                    throw new Error('Error logging in to' + peerToString(peer))
                }
            }

            if (!sessionCookie) {
                await login(peer)
            }

            let res = await postDataFn(peer, sessionCookie)

            if (res.data?.result === 'nouser' && sessionCookie) {
                await login(peer)

                res = await postDataFn(peer, sessionCookie)
            }

            if (
                res.error ||
                !res.data?.success ||
                res.data?.result === 'nouser'
            ) {
                results.errors.push(peer)
                console.error(
                    `Error posting data to ${peerToString(peer)}`,
                    res.error || JSON.stringify(res.data)
                )
            } else {
                results.sent.push(peer)
            }

            if (resultCallback) resultCallback(peer, res)
        } catch (e) {
            results.loginErrors.push(peer)
        }
    }

    const logMsg: string[] = []
    const addToLogMsg = (
        peerResult: PeerInfo[],
        prefix: string,
        color: string
    ) => {
        if (peerResult.length > 0) {
            logMsg.push(
                `${logger.C(color)}${prefix}:${logger.C()} ` +
                    peerResult.map((x) => peerToString(x)).join(', ')
            )
        }
    }
    addToLogMsg(results.loginErrors, 'Login error', 'red')
    addToLogMsg(results.errors, 'Error', 'red')
    addToLogMsg(results.sent, 'Sent', 'green')

    logger.Log(
        `\t${logger.C('green')}Sent data to peers${logger.C()}; ${logMsg.join(
            ', '
        )}`
    )
}

export async function loginAndDownloadFile(
    peer: PeerInfo,
    destination: string,
    fileName: string,
    dir: string
): Promise<{ success: boolean; message?: string }> {
    const download = (sessionCookie: string) => {
        return downloadFile(
            {
                host: peer.host,
                port: peer.port,
                path: `/api/userFiles/${encodeURIComponent(
                    dir
                )}/${encodeURIComponent(fileName)}`,
            },
            destination,
            `sessionID=${sessionCookie}`,
            peer.http
        )
    }

    try {
        let sessionCookie = peer.sessionCookie

        const login = async (peer: PeerInfo) => {
            const loginResult = await loginToPeer(peer)
            if (typeof loginResult === 'string') {
                sessionCookie = loginResult
                updatePeersFile(peer, { sessionCookie: loginResult })
            } else {
                throw new Error('Error logging in to' + peerToString(peer))
            }
        }

        if (!sessionCookie) {
            await login(peer)
        }

        let res = await download(sessionCookie)

        if (res.result === 'nouser' && sessionCookie) {
            await login(peer)

            res = await download(sessionCookie)
        } else if (!res.success) {
            throw new Error(res.message)
        }

        if (res.result === 'nouser') {
            throw new Error(`Unable to login to peer: ${peerToString(peer)}`)
        }
        return { success: true }
    } catch (e) {
        return { success: false, message: e.message }
    }
}
