/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

const DbStruct = {
    msgs: {
        tableStruct: {
            id: {
                type: 'integer',
                primary: true,
                autoIncrement: true,
            },
            sender: {
                type: 'integer',
                notNull: true,
            },
            reciever: {
                type: 'integer',
                notNull: true,
            },
            msg: {
                type: 'text',
            },
            type: {
                type: 'text',
            },
            date: {
                type: 'integer',
            },
            unread: {
                type: 'integer',
                defaultZero: true,
            },
        },
    },
}

exports.default = DbStruct
