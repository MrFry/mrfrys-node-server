// https://www.sqlitetutorial.net/sqlite-nodejs/
// https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md

const dbtools = require('../utils/dbtools.js')

Main()

function Main() {
    const cols = {
        uname: {
            type: 'text',
        },
        pw: {
            type: 'text',
        },
        notes: {
            type: 'text',
        },
    }
    const dbName = 'test'

    const db = dbtools.GetDB('./testdb.db')

    // Creating table
    dbtools.CreateTable(db, dbName, cols)
    console.log(dbtools.TableInfo(db, dbName))
    dbtools.SelectAll(db, dbName)
    // inserting test val to table
    dbtools.Insert(db, dbName, {
        uname: 'mrfry',
        pw: 'dsads',
    })
    // Selecting a record
    console.log(
        dbtools.Select(db, dbName, {
            uname: 'mrfry',
        })
    )
    console.log(dbtools.TableInfo(db, dbName))
    console.log(dbtools.SelectAll(db, dbName))
    // Updating record
    dbtools.Update(
        db,
        dbName,
        {
            pw: 'sspw',
        },
        {
            uname: 'mrfry',
        }
    )
    console.log(dbtools.SelectAll(db, dbName))
    // Updating record again
    dbtools.Update(
        db,
        dbName,
        {
            notes: 'new note!',
        },
        {
            uname: 'mrfry',
        }
    )
    console.log(dbtools.SelectAll(db, dbName))
    // Adding new column and
    dbtools.AddColumn(db, dbName, {
        test: 'text',
    })
    console.log(dbtools.TableInfo(db, dbName))
    console.log(dbtools.SelectAll(db, dbName))
    // Deleting stuff
    dbtools.Delete(db, dbName, {
        uname: 'mrfry',
    })
    console.log(dbtools.SelectAll(db, dbName))

    dbtools.CloseDB(db)
}
