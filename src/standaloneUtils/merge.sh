#!/bin/bash

p=$(echo $PWD | rev | cut -d'/' -f2- | rev)

cp -v $p/public/data.json /tmp/data.json
node $p/utils/rmDuplicates.js /tmp/data.json 2> /dev/null

mkdir -p "$p/public/backs/"
mv -v $p/public/data.json "$p/public/backs/data.json $(date)"
mv -v $p/utils/res.json $p/public/data.json

echo Done
