#!/bin/bash

if [ "$#" -lt "2" ]; then
  echo "No params! 2 file required: db, commands file"
  echo "usage: ./runSqliteCmds db.db commands"
  exit 1
fi

echo "Executing:"
cat $2
echo

cmd=''

while read p; do
  cmd="$cmd -cmd \"${p}\" -cmd \".shell echo\""
done <"$2"

echo "sqlite3 -bail $1 $cmd"
eval "sqlite3 -bail $1 $cmd" > cmdRes 2> /dev/null

echo "Done, result written to cmdRes file!"
