const fs = require('fs')
const { simplifyString } = require('./src/utils/classes.js')

const file = './publicDirs/qminingPublic/data.json'

const data = JSON.parse(fs.readFileSync(file, 'utf8'))
const res = []

data.forEach((subj) => {
    const questions = []
    subj.Questions.forEach((question) => {
        const res = {}
        if (question.Q) {
            res.Q = simplifyString(question.Q)
        }
        if (question.A) {
            res.A = simplifyString(question.A)
        }
        res.data = question.data

        questions.push(res)
    })
    res.push({
        Name: subj.Name,
        Questions: questions,
    })
})

fs.writeFileSync(file + '.res', JSON.stringify(res))
