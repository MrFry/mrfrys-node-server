const fs = require('fs')

const params = process.argv
const file = params[2]

const data = fs.readFileSync(file, 'utf8').split('\n')
console.log(data)

console.log("TODO: remove 'Q: ' and 'A: '")

let currVal = {}
const res = data.reduce((acc, val) => {
    const formattedVal = val.replace(/\r/g, '').trim()

    if (formattedVal.startsWith('#')) return acc
    if (formattedVal.startsWith('Q')) {
        currVal = {
            Q: formattedVal,
        }
        return acc
    }
    if (formattedVal.startsWith('A')) {
        currVal.A = formattedVal
        return [
            ...acc,
            {
                ...currVal,
                data: {
                    type: 'simple',
                },
            },
        ]
    }

    return acc
}, [])

console.log(res)
fs.writeFileSync('./res.json', JSON.stringify(res, null, 2))
console.log('DONE')
