const utils = require('../../dist/utils/utils.js').default // eslint-disable-line
// TODO: logger creates stat dir in pwd
const logger = require('../../dist/utils/logger.js').default // eslint-disable-line
const dbtools = require('../../dist/utils/dbtools.js').default // eslint-disable-line
const { v4: uuidv4 } = require('uuid') // eslint-disable-line

const dbStructPaths = [
    { structPath: '../../src/modules/api/usersDBStruct.js', name: 'users.db' },
    { structPath: '../../src/modules/api/msgsDbStruct.js', name: 'msgs.db' },
]

dbStructPaths.forEach((data) => {
    const { structPath, name } = data
    createDB(structPath, name)
})

function createDB(path, name) {
    console.log(path, name)
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const dbStruct = require(path).default
    const db = dbtools.GetDB(`./${name}`)
    db.pragma('synchronous = OFF')

    Object.keys(dbStruct).forEach((tableName) => {
        const tableData = dbStruct[tableName]
        logger.Log(`Creating table ${tableName} ...`)
        dbtools.CreateTable(
            db,
            tableName,
            tableData.tableStruct,
            tableData.foreignKey
        )
    })
    // logger.Log(`${name} db info:`)
    // printDb(db, dbStruct)
    db.close()

    logger.Log(`Created db ${name} at ${path}`)
}

function printDb(db, dbStruct) {
    Object.keys(dbStruct).forEach((key) => {
        console.log(dbtools.TableInfo(db, key))
        console.log(dbtools.SelectAll(db, key))
    })
}
