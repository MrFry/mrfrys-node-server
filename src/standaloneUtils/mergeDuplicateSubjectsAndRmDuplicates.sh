#!/bin/bash

OUTDIR="/tmp/res/"

echo "> pwd ${PWD}"

mkdir -p OUTDIR
node ./src/standaloneUtils/duplicateSubjectsInQdbFix.js ./public/questionDbs/ "${OUTDIR}"

echo -e "\n\n\n"
find "${OUTDIR}" -type f | while IFS= read -r qdb; do 
    node  \
        ./src/standaloneUtils/rmDuplicates.js \
        "${qdb}"

    echo -e "\n\n\n"
done

rm -v ${OUTDIR}*.json || exit

mv -v ./*.res "${OUTDIR}" || exit

find "${OUTDIR}" -type f | while IFS= read -r qdb; do 
    newName=$(echo "${qdb}" | rev | cut -d '.' -f 2- | rev)
    mv -v "${qdb}" "${newName}"
done

echo "Done!"
