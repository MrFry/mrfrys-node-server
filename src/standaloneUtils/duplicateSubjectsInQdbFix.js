/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs')
const path = require('path')

const filesDirPath = process.argv[2]
const resdir = process.argv[3]
const files = fs.readdirSync(filesDirPath)

let duplicateSubjects = 0
let questionsInDuplicateSubjects = 0

if (!fs.existsSync(resdir)) {
    fs.mkdirSync(resdir)
}

function merge(x, y) {
    return {
        Name: x.Name,
        Questions: [...x.Questions, ...y.Questions],
    }
}

const fixQdb = (dbname) => {
    console.log(dbname)
    console.log()
    const file = JSON.parse(
        fs.readFileSync(path.join(filesDirPath + dbname), 'utf8')
    )

    const result = {}

    file.forEach((x) => {
        if (result[x.Name]) {
            console.log(x.Name, ' is duplicate!')
            duplicateSubjects += 1
            result[x.Name] = merge(result[x.Name], x)
            questionsInDuplicateSubjects += result[x.Name].Questions.length
        } else {
            result[x.Name] = x
        }
    })

    const newFile = Object.values(result)

    fs.writeFileSync(path.join(resdir, dbname), JSON.stringify(newFile))
}

if (fs.existsSync(resdir)) {
    fs.rmSync(resdir, { recursive: true })
    fs.mkdirSync(resdir)
}

files.forEach((x) => {
    if (x.includes('json')) {
        fixQdb(x)
    }
})

console.log('Done! Found duplicates:', duplicateSubjects)
console.log(questionsInDuplicateSubjects)
