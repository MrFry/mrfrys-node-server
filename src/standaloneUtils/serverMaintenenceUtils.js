/* eslint-disable @typescript-eslint/no-var-requires */
const { countQuestionsInSubjects } = require('../../dist/utils/qdbUtils.js')
const fs = require('fs')

const command = process.argv[2]
const args = process.argv.slice(3)

const actions = {
    qdbcount: () => {
        const qdb = JSON.parse(fs.readFileSync(args[0], 'utf-8'))
        const questionCount = countQuestionsInSubjects(qdb)
        console.log({ questionCount: questionCount })
    },
}

if (actions[command]) {
    actions[command]()
} else {
    console.log('No action for ' + command)
    console.log('Possible commands: ', Object.keys(actions))
}
