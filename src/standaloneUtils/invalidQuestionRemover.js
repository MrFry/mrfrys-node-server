const fs = require('fs')

function GetParams() {
    return process.argv.splice(2)
}
const params = GetParams()
console.log(params)
if (params.length === 0) {
    console.error('No params! Need a path to a question database!')
    process.exit()
}
const file = params[0]

const data = JSON.parse(fs.readFileSync(file, 'utf8'))
const res = []
let invalidQuestionCount = 0

data.forEach((subj) => {
    const questions = []
    subj.Questions.forEach((question) => {
        if (isInvalidQuestion(question)) {
            console.log(`invalid question in ${subj.Name}:`)
            console.log(question)
            invalidQuestionCount++
        } else {
            questions.push(question)
        }
    })
    res.push({
        Name: subj.Name,
        Questions: questions,
    })
})

function isInvalidQuestion(q) {
    if (q.Q === 'Ugrás...' || q.A === 'Ugrás...') {
        return true
    }

    if (!q.Q && !q.A) {
        return true
    }

    if (!q.Q && q.data.type === 'simple') {
        return true
    }

    return false
}

console.log(`${invalidQuestionCount} invalid questions, writing results...`)
fs.writeFileSync(file + '.res', JSON.stringify(res))

console.log('Done')
