/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

import { Crypt, RSA } from 'hybrid-crypto-js'

const rsa = new RSA()
const crypt = new Crypt()

export const createKeyPair = (): Promise<{
    publicKey: string
    privateKey: string
}> => {
    return rsa.generateKeyPairAsync()
}

export const encrypt = (publicKey: string, text: string): string => {
    return crypt.encrypt(publicKey, text)
}

export const decrypt = (privateKey: string, text: string): string => {
    return crypt.decrypt(privateKey, text).message
}

export const isKeypairValid = (
    publicKey: string,
    privateKey: string
): boolean => {
    const testText = 'nem volt jobb ötletem na'
    try {
        const encryptedText = encrypt(publicKey, testText)
        const decryptedText = decrypt(privateKey, encryptedText)
        return decryptedText === testText
    } catch (e) {
        return false
    }
}
