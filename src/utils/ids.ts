/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

export default {
    LogId: LogId,
    Load: Load,
}

import utils from '../utils/utils'
import logger from '../utils/logger'

import { paths } from './files'

// TODO: on process exit write all
const writeInterval = 20

let idStatsData = {}
let idvStatsData = {}
let writes = 0

function Load(): void {
    if (utils.FileExists(paths.idstatsFile)) {
        idStatsData = utils.ReadJSON(paths.idstatsFile)
    }

    if (utils.FileExists(paths.idvstatsFile)) {
        idvStatsData = utils.ReadJSON(paths.idvstatsFile)
    }
}

function LogId(
    id: number,
    subj: string,
    newQuestions: number,
    allQuestions: number
): void {
    Inc(id, subj, newQuestions, allQuestions)
    AddVisitStat(id, subj, newQuestions, allQuestions)
    Save()
}

function AddSubjToList(list: { [key: string]: any }, subj: string) {
    if (!list[subj]) {
        list[subj] = 0
    }
    list[subj]++
}

function Inc(
    value: number,
    subj: string,
    newQuestions: number,
    allQuestions: number
) {
    if (typeof idStatsData[value] !== 'object' || idStatsData[value] === null) {
        idStatsData[value] = {
            count: 0,
            newQuestions: 0,
            allQuestions: 0,
            subjs: {},
        }
    }
    idStatsData[value].count++
    idStatsData[value].newQuestions += newQuestions
    idStatsData[value].allQuestions += allQuestions
    AddSubjToList(idStatsData[value].subjs, subj)
}

function AddVisitStat(
    name: number,
    subj: string,
    newQuestions: number,
    allQuestions: number
) {
    const date = new Date()
    const now =
        date.getFullYear() +
        '-' +
        ('0' + (date.getMonth() + 1)).slice(-2) +
        '-' +
        ('0' + date.getDate()).slice(-2)
    if (typeof idvStatsData[now] !== 'object' || idvStatsData[now] === null) {
        idvStatsData[now] = {}
    }

    if (
        typeof idvStatsData[now][name] !== 'object' ||
        idvStatsData[now][name] === null
    ) {
        idvStatsData[now][name] = {
            count: 0,
            newQuestions: 0,
            allQuestions: 0,
            subjs: {},
        }
    }
    idvStatsData[now][name].count++
    idvStatsData[now][name].newQuestions += newQuestions
    idvStatsData[now][name].allQuestions += allQuestions
    AddSubjToList(idvStatsData[now][name].subjs, subj)
}

function Save() {
    writes++
    if (writes === writeInterval) {
        try {
            utils.WriteFile(JSON.stringify(idStatsData), paths.idstatsFile)
            // Log("Stats wrote.");
        } catch (err) {
            logger.Log('Error at writing logs!', logger.GetColor('redbg'))
            console.error(err)
        }
        try {
            utils.WriteFile(JSON.stringify(idvStatsData), paths.idvstatsFile)
            // Log("Stats wrote.");
        } catch (err) {
            logger.Log('Error at writing visit logs!', logger.GetColor('redbg'))
            console.error(err)
        }
        writes = 0
    }
}
