import http, { IncomingMessage, request as httpRequest } from 'http'
import https, { request as httpsRequest } from 'https'
import fs from 'node:fs'
import utils from './utils'

export interface GetResult<T> {
    data?: T
    error?: Error
    options?: http.RequestOptions
}

export function getRaw(
    options: http.RequestOptions,
    useHttp?: boolean
): Promise<GetResult<Buffer> & { response?: IncomingMessage }> {
    const provider = useHttp ? http : https

    return new Promise((resolve) => {
        const req = provider.get(options, function (res) {
            const bodyChunks: Uint8Array[] = []
            res.on('data', (chunk) => {
                bodyChunks.push(chunk)
            }).on('end', () => {
                const body = Buffer.concat(bodyChunks)
                try {
                    if (res.statusCode === 200) {
                        resolve({ data: body, response: res })
                    } else {
                        resolve({
                            data: body,
                            error: new Error(
                                `HTTP response code: ${res.statusCode}`
                            ),
                            response: res,
                        })
                    }
                } catch (e) {
                    resolve({ error: e, options: options, response: res })
                }
            })
        })
        req.on('error', function (e) {
            resolve({ error: e, options: options })
        })
    })
}

export async function get<T = any>(
    options: http.RequestOptions,
    useHttp?: boolean
): Promise<GetResult<T>> {
    const { data, response } = await getRaw(
        {
            ...options,
            headers: {
                'Content-Type': 'application/json',
                ...options?.headers,
            },
        },
        useHttp
    )

    const body = data.toString()

    try {
        if (response.statusCode === 200) {
            return { data: JSON.parse(body) }
        } else {
            return {
                data: JSON.parse(body),
                error: new Error(`HTTP response code: ${response.statusCode}`),
            }
        }
    } catch (e) {
        return { error: e, options: options }
    }
}

export interface PostResult<T> {
    data?: T
    error?: Error
    cookie?: string[]
}

interface PostParams {
    hostname: string
    path: string
    port: number
    bodyObject: any
    http?: boolean
    cookie?: string
}

// https://nodejs.org/api/http.html#httprequesturl-options-callback
export function post<T = any>({
    hostname,
    path,
    port,
    bodyObject,
    http,
    cookie,
}: PostParams): Promise<PostResult<T>> {
    const provider = http ? httpRequest : httpsRequest
    const body = JSON.stringify(bodyObject)

    return new Promise((resolve) => {
        const req = provider(
            {
                hostname: hostname,
                port: port,
                path: path,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(body),
                    ...(cookie
                        ? {
                              cookie: cookie,
                          }
                        : {}),
                },
            },
            (res) => {
                const bodyChunks: string[] = []
                res.setEncoding('utf8')
                res.on('data', (chunk) => {
                    bodyChunks.push(chunk)
                })
                res.on('end', () => {
                    const body = bodyChunks.join()
                    try {
                        resolve({
                            data: JSON.parse(body),
                            cookie: res.headers['set-cookie'],
                        })
                    } catch (e) {
                        resolve({ error: e })
                    }
                })
            }
        )

        req.on('error', (e) => {
            resolve({ error: e })
        })

        req.end(body)
    })
}

export function downloadFile(
    options: http.RequestOptions,
    destination: string,
    cookie: string,
    useHttp?: boolean
): Promise<{ message?: string; result?: string; success: boolean }> {
    const provider = useHttp ? http : https

    if (utils.FileExists(destination)) {
        return Promise.resolve({
            success: true,
            message: `\tDownload file: "${destination}" already esxists, skipping download`,
        })
    }

    return new Promise((resolve, reject) => {
        provider.get(
            {
                ...options,
                headers: {
                    'Content-Type': 'application/json',
                    ...options?.headers,
                    ...(cookie
                        ? {
                              cookie: cookie,
                          }
                        : {}),
                },
            },
            function (res) {
                if (res.statusCode === 200) {
                    utils.createDirsForFile(destination)
                    const file = fs.createWriteStream(destination)

                    res.pipe(file)

                    file.on('finish', () => {
                        file.close()
                        resolve({ success: true })
                    })

                    res.on('error', (e) => {
                        file.close()
                        utils.deleteFile(destination)
                        reject(e)
                    })
                } else if (res.statusCode === 401) {
                    resolve({ success: false, result: 'nouser' })
                } else {
                    resolve({
                        success: false,
                        message: `Unhandled status code: ${res.statusCode}`,
                    })
                }
            }
        )
    })
}

export function parseCookie(responseCookie: string[]): {
    [key: string]: string
} {
    const cookieArray = responseCookie.join('; ').split('; ')
    const parsedCookie: { [key: string]: string } = cookieArray.reduce(
        (acc, cookieString) => {
            const [key, val] = cookieString.split('=')
            return {
                ...acc,
                [key]: val || true,
            }
        },
        {}
    )
    return parsedCookie
}
