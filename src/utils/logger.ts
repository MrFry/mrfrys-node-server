/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

const hr =
    '---------------------------------------------------------------------------------'

const DELIM = C('green') + '|' + C()

// import express from 'express'
import utils from '../utils/utils'
import { Request } from '../types/basicTypes'
import { paths } from './files'

const colors = ['green', 'red', 'yellow', 'blue', 'magenta', 'cyan']
const logFileName = 'log'

// TODO: on process exit write all
const writeInterval = 10

let vData = {} // visit data
let dvData = {} // visit data, but daily
let uvData = {} // visit data, but per user
let udvData = {} // visit data, but per user and daily
let writes = 0
let loggingDisabled = false

let noLogIds: string[] = []

function setLoggingDisabled(newVal: boolean): void {
    loggingDisabled = newVal
}

function getColoredDateString(): string {
    const date = new Date()
    const dateString = utils.GetDateString()
    return GetRandomColor(date.getHours().toString()) + dateString + C()
}

function Log(msg: string | object, color?: string): void {
    if (loggingDisabled) return

    let log = msg
    if (typeof msg !== 'object') {
        const delimiter = DELIM + C(color)
        log = getColoredDateString() + delimiter + C(color) + msg + C()
    }

    if (!process.env.NS_NOLOG) {
        console.log(log)
    }
    utils.AppendToFile(
        typeof log === 'string' ? log : JSON.stringify(log),
        paths.logDir + logFileName
    )
}

function expandWithSpaces(text: string, count: number) {
    while (text.length < count) {
        text += ' '
    }
    return text
}

function LogReq(
    req: Request,
    toFile?: boolean,
    statusCode?: string | number
): void {
    try {
        let logEntry = '' // logHashed(ip)
        let dl = DELIM
        if (req.url.includes('lred')) {
            dl += C('red')
        }
        if (
            req.session &&
            req.session.user &&
            !shouldLog(req.session.user.id, noLogIds)
        ) {
            return
        }

        if (toFile) {
            logEntry += req.headers['user-agent'] + dl
            logEntry += req.method + dl
        }

        let uid = ''
        if (req.session && req.session.user) {
            uid = req.session.user.id.toString()
        } else if (req.session && req.session.isException === true) {
            uid = 'EX'
        } else {
            uid = 'NOUSR'
        }
        if (!toFile) {
            uid = expandWithSpaces(uid, 5)
        }
        logEntry += GetRandomColor(uid.toString()) + uid + C() + dl

        logEntry +=
            GetRandomColor(req.originalUrl.split('?')[0]) +
            req.originalUrl +
            C()

        if (statusCode !== undefined) {
            logEntry += dl + statusCode
        }

        logEntry += C()
        if (!toFile) {
            Log(logEntry)
        } else {
            const defLogs = utils.GetDateString() + dl + logEntry

            utils.AppendToFile(defLogs, paths.vlogDir + logFileName)
        }
    } catch (err) {
        console.error(err)
        Log('Error at logging lol', GetColor('redbg'))
    }
}

function parseNoLogFile(newData: string) {
    noLogIds = newData.split('\n')
    if (noLogIds[noLogIds.length - 1] === '') {
        noLogIds.pop()
    }
    noLogIds = noLogIds.filter((noLogId) => {
        return noLogId !== ''
    })
}

function setNoLogReadInterval() {
    utils.WatchFile(paths.nologFile, (newData: string) => {
        parseNoLogFile(newData)
        Log('No Log user ID-s changed: ' + noLogIds.join(', '))
    })

    parseNoLogFile(utils.ReadFile(paths.nologFile))
}

function Load(): void {
    if (utils.FileExists(paths.uStatsFile)) {
        uvData = utils.ReadJSON(paths.uStatsFile)
    }

    if (utils.FileExists(paths.uvStatsFile)) {
        udvData = utils.ReadJSON(paths.uvStatsFile)
    }

    if (utils.FileExists(paths.statFile)) {
        vData = utils.ReadJSON(paths.statFile)
    }

    if (utils.FileExists(paths.vStatFile)) {
        dvData = utils.ReadJSON(paths.vStatFile)
    }
    setNoLogReadInterval()
}

export function shouldLog(userId: string | number, nolog: string[]): boolean {
    return !nolog.some((noLogId) => {
        return noLogId === userId.toString()
    })
}

function LogStat(url: string, hostname: string, userId: number | string): void {
    if (!shouldLog(userId, noLogIds)) {
        return
    }

    url = hostname + url.split('?')[0]
    Inc(url)
    AddVisitStat(url)
    if (shouldAddUserStat(url)) {
        AddUserIdStat(userId.toString())
        IncUserStat(userId.toString())
    }
    Save()
}

const userStatExcludes = ['stable.user.js', 'infos', 'hasNewMsg']
function shouldAddUserStat(url: string) {
    return !userStatExcludes.some((x) => url.includes(x))
}

function IncUserStat(userId: string) {
    try {
        if (uvData[userId] === undefined) {
            uvData[userId] = 0
        }
        uvData[userId]++
    } catch (err) {
        Log('Error at making user ID stats!', GetColor('redbg'))
        console.error(err)
    }
}

function AddUserIdStat(userId: string) {
    try {
        const date = new Date()
        const now =
            date.getFullYear() +
            '-' +
            ('0' + (date.getMonth() + 1)).slice(-2) +
            '-' +
            ('0' + date.getDate()).slice(-2)
        if (udvData[now] === undefined) {
            udvData[now] = {}
        }
        if (udvData[now][userId] === undefined) {
            udvData[now][userId] = 0
        }
        udvData[now][userId]++
    } catch (err) {
        Log('Error at making user ID stats!', GetColor('redbg'))
        console.error(err)
    }
}

function Inc(value: string) {
    if (value.startsWith('/?')) {
        value = '/'
    }
    if (vData[value] === undefined) {
        vData[value] = 0
    }
    vData[value]++
}

function AddVisitStat(name: string) {
    const date = new Date()
    const now =
        date.getFullYear() +
        '-' +
        ('0' + (date.getMonth() + 1)).slice(-2) +
        '-' +
        ('0' + date.getDate()).slice(-2)
    if (dvData[now] === undefined) {
        dvData[now] = {}
    }
    if (dvData[now][name] === undefined) {
        dvData[now][name] = 0
    }
    dvData[now][name]++
}

function Save() {
    writes++
    if (writes === writeInterval) {
        try {
            utils.WriteFile(JSON.stringify(uvData), paths.uStatsFile)
        } catch (err) {
            Log('Error at writing logs! (more in stderr)', GetColor('redbg'))
            console.error(err)
        }
        try {
            utils.WriteFile(JSON.stringify(udvData), paths.uvStatsFile)
        } catch (err) {
            Log('Error at writing logs! (more in stderr)', GetColor('redbg'))
            console.error(err)
        }
        try {
            utils.WriteFile(JSON.stringify(vData), paths.statFile)
            // Log("Stats wrote.");
        } catch (err) {
            Log('Error at writing logs! (more in stderr)', GetColor('redbg'))
            console.error(err)
        }
        try {
            utils.WriteFile(JSON.stringify(dvData), paths.vStatFile)
            // Log("Stats wrote.");
        } catch (err) {
            Log(
                'Error at writing visit logs! (more in stderr)',
                GetColor('redbg')
            )
            console.error(err)
        }
        writes = 0
    }
}

function logHashed(msg: string): string {
    return GetRandomColor(msg.toString()) + msg + C()
}

function GetRandomColor(msg: string): string {
    if (!msg) {
        return 'red'
    }

    const res = msg.split('').reduce((res, character) => {
        return res + character.charCodeAt(0)
    }, 0)
    return C(colors[res % colors.length])
}

function GetColor(color: string): string {
    return color
}

function C(color?: string): string {
    if (color !== undefined) {
        color = color.toLowerCase()
    }

    if (color === 'redbg') {
        return '\x1b[41m'
    }
    if (color === 'yellowbg') {
        return '\x1b[43m\x1b[30m'
    }
    if (color === 'bluebg') {
        return '\x1b[44m'
    }
    if (color === 'cyanbg') {
        return '\x1b[46m'
    }
    if (color === 'green') {
        return '\x1b[32m'
    }
    if (color === 'red') {
        return '\x1b[31m'
    }
    if (color === 'yellow') {
        return '\x1b[33m'
    }
    if (color === 'blue') {
        return '\x1b[34m'
    }
    if (color === 'magenta') {
        return '\x1b[35m'
    }
    if (color === 'cyan') {
        return '\x1b[36m'
    }
    return '\x1b[0m'
}

function logTable(
    table: (string | number)[][],
    options: { colWidth?: number[]; rowPrefix?: string } = {}
): void {
    const { colWidth, rowPrefix } = options

    table.forEach((row, i) => {
        const rowString: string[] = []
        row.forEach((cell, j) => {
            const cellColor = j === 0 || i === 0 ? 'blue' : 'green'
            let cellVal = ''
            if (!isNaN(+cell)) {
                cellVal = cell.toLocaleString()
            } else {
                cellVal = cell.toString()
            }

            if (colWidth && colWidth[j] && colWidth[j] !== -1) {
                if (cellVal.length < colWidth[j]) {
                    while (cellVal.length < colWidth[j]) {
                        cellVal += ' '
                    }
                } else if (cellVal.length > colWidth[j]) {
                    cellVal = cellVal.substring(0, colWidth[j] - 3) + '...'
                }
            }

            rowString.push(C(cellColor) + cellVal + C())
        })
        Log((rowPrefix || '') + rowString.join('\t'))
    })
}

export default {
    getColoredDateString: getColoredDateString,
    Log: Log,
    GetColor: GetColor,
    LogReq: LogReq,
    LogStat: LogStat,
    Load: Load,
    logHashed: logHashed,
    hr: hr,
    C: C,
    logFileName: logFileName,
    setLoggingDisabled: setLoggingDisabled,
    logTable: logTable,
}
