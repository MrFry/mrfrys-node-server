/* ----------------------------------------------------------------------------

 Question Server
 GitLab: <https://gitlab.com/MrFry/mrfrys-node-server>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

import express from 'express'
import type { Database } from 'better-sqlite3'
import type { Socket as SocketIoSocket } from 'socket.io'
import http from 'http'
import https from 'https'
import { SearchResultQuestion } from '../utils/qdbUtils'

export interface QuestionData {
    type: string
    date?: number
    images?: Array<string>
    hashedImages?: Array<string>
    possibleAnswers?: Array<{
        type: string
        val: string
        selectedByUser?: boolean
    }>
    base64?: string[]
    source?: string
}

export interface Question {
    Q: string
    A: string
    data: QuestionData
    cache?: {
        Q: Array<string>
        A: Array<string>
    }
}

export interface Subject {
    Name: string
    Questions: Array<Question>
}

export interface DataFile {
    path: string
    name: string
    locked?: Boolean
    overwrites?: Array<{
        subjName: string
        overwriteBeforeDate: number
    }>
    shouldSearch:
        | string
        | {
              location?: {
                  val: string
              }
          }
    shouldSave: {
        location?: {
            val: string
        }
        version?: {
            compare: string
            val: string
        }
    }
}

export interface QuestionDb extends DataFile {
    data: Array<Subject>
    index: number
}

export interface User {
    id: number
    pw: string
    notes?: string
    loginCount?: number
    avaiblePWRequests: number
    pwRequestCount?: number
    createdBy: number
    created: number
    lastLogin?: number
    lastAccess?: number
    sourceHost?: string
    //     isAdmin: boolean // TODO
}

export interface Request<T = any> extends express.Request {
    body: T
    cookies: any
    session: {
        user?: User
        sessionID?: string
        isException?: boolean
    }
    files: any
    query: { [key: string]: string }
}

export interface ModuleSpecificData {
    // TODO: rename to something more meaningfull
    setQuestionDbs: (newVal: QuestionDb[]) => void
    getQuestionDbs: () => QuestionDb[]
    dbsFile: string
}

export interface SubmoduleData {
    app: express.Application
    userDB?: Database
    nextdir?: string
    moduleSpecificData: ModuleSpecificData
    httpServer: http.Server
    httpsServer: https.Server
}

export interface QuestionFromScript {
    questions: Array<Question>
    testUrl: string
    subj: string
}

export interface DbSearchResult {
    message?: string
    recievedData?: string
    question: Question
    result: SearchResultQuestion[]
    success: boolean
}

export interface RegisteredUserEntry {
    cid: string
    version: string
    installSource: string
    date: string
    userAgent: string
    loginDate?: string
    uid?: number
}

export interface Submodule {
    dailyAction?: () => void
    load?: () => void
}

export interface ModuleType {
    app: express.Application
    dailyAction?: Function
}

export interface Socket extends SocketIoSocket {
    user: User
}

export interface PeerInfo {
    name: string
    host: string
    port: number
    publicKey: string
    contact: string
    pw?: string
    sessionCookie?: string
    lastSync?: number
    lastQuestionsSync?: number
    lastUsersSync?: number
    lastUserFilesSync?: number
    note?: string
    http?: boolean
}

export interface HttpsFiles {
    privkeyFile: string
    fullchainFile: string
    chainFile: string
}
