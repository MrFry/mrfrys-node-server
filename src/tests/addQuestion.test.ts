import { Subject, Question } from '../types/basicTypes'
import fs from 'fs'
import { RecievedData } from '../utils/actions'
import {
    addQuestion,
    createQuestion,
    getSubjNameWithoutYear,
} from '../utils/qdbUtils'

const question: Question = createQuestion('asd', 'asd', { type: 'simple' })

test('Adds questions to empty db', () => {
    const emptyDb: Subject[] = []
    addQuestion(emptyDb, 'test subject', question)
    expect(emptyDb.length).toBe(1)
})

test('Adds questions next to existing', () => {
    const db: Subject[] = [
        {
            Name: 'test subject',
            Questions: [question],
        },
    ]
    addQuestion(db, 'another something', question)
    expect(db.length).toBe(2)
})

test('Does not add new subject, multiple new questions', () => {
    const db: Subject[] = [
        {
            Name: 'test subject',
            Questions: [question],
        },
    ]
    addQuestion(db, 'test subject', question)
    addQuestion(db, 'test subject', question)
    addQuestion(db, 'test subject', question)
    addQuestion(db, 'test subject', question)
    expect(db.length).toBe(1)
})

test('Adds new subjects, multiple new questions', () => {
    const db: Subject[] = [
        {
            Name: 'test subject',
            Questions: [question],
        },
    ]
    addQuestion(db, 'gfjdkglfd', question)
    addQuestion(db, ' somrthing test ', question)
    addQuestion(db, 'aaaaaaaa', question)
    expect(db.length).toBe(4)
})

test("New subject names shouldn't be empty", () => {
    const data: RecievedData = JSON.parse(
        fs.readFileSync(
            __dirname + '/../../testingTools/tests/testData/ROS.json',
            'utf8'
        )
    )

    const db: Subject[] = [
        {
            Name: 'test subject',
            Questions: [question],
        },
    ]

    data.quiz.forEach((question) => {
        const subjName = getSubjNameWithoutYear(data.subj)
        addQuestion(db, subjName, question)
    })
    db.forEach((subj) => {
        expect(subj.Name).toBeTruthy()
    })
})

test('New questions shouldnt be added to accidental empty named subjects', () => {
    const data: RecievedData = JSON.parse(
        fs.readFileSync(
            __dirname + '/../../testingTools/tests/testData/ROS.json',
            'utf8'
        )
    )

    const db: Subject[] = [
        {
            Name: '',
            Questions: [question],
        },
    ]

    data.quiz.forEach((question) => {
        const subjName = getSubjNameWithoutYear(data.subj)
        addQuestion(db, subjName, question)
    })
    expect(db[0].Questions.length).toBe(1)
    expect(db.length).toBe(2)
})

test('Question gets added to the correct subject', () => {
    const data: RecievedData = JSON.parse(
        fs.readFileSync(
            __dirname + '/../../testingTools/tests/testData/ROS.json',
            'utf8'
        )
    )

    const subjName = getSubjNameWithoutYear(data.subj)
    const db: Subject[] = [
        {
            Name: subjName,
            Questions: [question],
        },
        {
            Name: data.subj, // subj name with course date in it (2022/23/2 -)
            Questions: [question],
        },
    ]

    data.quiz.forEach((question) => {
        addQuestion(db, subjName, question)
    })
    data.quiz.forEach((question) => {
        addQuestion(db, 'some other subject name', question)
    })

    expect(db[0].Questions.length).toBe(9)
    expect(db[1].Questions.length).toBe(1)
    expect(db[2].Questions.length).toBe(8)
    expect(db.length).toBe(3)
})
