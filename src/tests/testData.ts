import { QuestionDb } from '../types/basicTypes'
import { createQuestion } from '../utils/qdbUtils'

const rawQuestions = [
    // 0
    {
        Q: 'A kötvény és a részvény közös tulajdonsága, hogy TOREMOVE',
        A: 'piaci áruk eltérhet a névértéktől.',
        data: {
            type: 'simple',
            date: 1678692844547,
        },
    },
    // 1
    {
        Q: 'A kötvény és a részvény közös tulajdonsága, hogy TOREMOVE',
        A: 'afjléa gféda gfdjs légf',
        data: {
            type: 'simple',
            date: 1678692844547,
        },
    },
    // 2
    {
        Q: 'A kötvény és a részvény közös tulajdonsága, hogy TOREMOVE',
        A: 'afjlsd gfds dgfs gf  sdgf d',
        data: {
            type: 'simple',
            date: 1678692844547,
        },
    },
    // 3
    {
        Q: 'A kötvény névértéke',
        A: 'A kötvényen feltüntetett meghatározott nagyságú összeg.',
        data: {
            type: 'simple',
            date: 1678692844547,
        },
    },
    // 4
    {
        Q: 'Mi az osztalék? asd asd',
        A: 'A vállalati profit egy része..',
        data: {
            type: 'simple',
            date: 1678692844547,
        },
    },
    // 5
    {
        Q: 'valaim nagyon értelmes kérdés asd asd',
        A: 'A vállalati profit egy része..',
        data: {
            type: 'simple',
            date: 1678692844547,
        },
    },
    // 6
    {
        Q: 'A kötvény és a részvény közös tulajdonsága, hogy',
        A: 'piaci áruk eltérhet a névértéktől.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1252626725558,
        },
    },
    // 7
    {
        Q: 'A kötvény és a részvény közös tulajdonsága, hogy',
        A: 'afjléa gféda gfdjs légf',
        data: {
            type: 'simple',
            source: 'script',
            date: 1252626725558,
        },
    },
    // 8
    {
        Q: 'A kötvény névértéke',
        A: 'A kötvényen feltüntetett meghatározott nagyságú összeg.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1252626725558,
        },
    },
    // 9
    {
        Q: 'A részvényesnek joga van',
        A: 'Mind a háromra feljogosít.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725558,
        },
    },
    // 10
    {
        Q: 'Mi az osztalék?',
        A: 'A vállalati profit egy része..',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725559,
        },
    },
    // 11
    {
        Q: 'Az alábbi értékpapírok közül melyik kizárólagos kibocsátója a hitelintézet?',
        A: 'letéti jegy.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725559,
        },
    },
    // 12
    {
        Q: 'Mely állítás nem igaz a kötvényre?',
        A: 'az osztalék-számítás módját fel kell tüntetni az értékpapíron.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725559,
        },
    },
    // 13
    {
        Q: 'Mely állítás nem igaz a kötvényre?',
        A: 'tagsági jogot megtestesítő értékpapír.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725559,
        },
    },
    {
        Q: 'Az osztalék közvetlenül nem függ',
        A: 'a részvénytársaság múltbeli költséggazdálkodásától.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725560,
        },
    },
    {
        Q: 'Ha a részvénytársaság az egyik tulajdonosától visszavásárolja a saját részvényeit, akkor ezzel',
        A: 'átrendezi a vállalat tulajdonosi szerkezetét.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725560,
        },
    },
    {
        Q: 'Válassza ki az értékpapírokra vonatkozó helyes megállapítást!',
        A: 'Vagyoni jogot megtestesítő forgalomképes okirat.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725560,
        },
    },
    {
        Q: '10. Válassza ki, hogy mely értékpapírtípus járul hozzá egy vállalat alaptőkéjéhez?',
        A: 'Részesedési jogot megtestesítő értékpapír.',
        data: {
            type: 'simple',
            source: 'script',
            date: 1652636725560,
        },
    },
    {
        Q: 'When does the ~/.bashrc script run automatically?',
        A: 'When a new terminal window is opened..',
        data: {
            type: 'simple',
            source: 'script',
            date: 1678692844546,
        },
    },
    {
        Q: 'A robot is ...',
        A: '... a complex mechatronic system enabled with electronics, sensors, actuators and software, executing tasks with a certain degree of autonomy. It may be preprogrammed, teleoperated or carrying out computations to make decisions. .',
        data: {
            type: 'simple',
            source: 'script',
            date: 1678692844546,
        },
    },
    {
        Q: 'A robot is ...',
        A: '... some sort of device, which has sensors those sensors the world, does some sort of computation, decides on an action, and then does that action based on the sensory input, which makes some change out in the world, outside its body. .',
        data: {
            type: 'simple',
            source: 'script',
            date: 1678692844546,
        },
    },
    {
        Q: 'A robot is ...',
        A: '... an actuated mechanism programmable in two or more axes with a degree of autonomy, moving within its environment, to perform intended tasks. .',
        data: {
            type: 'simple',
            source: 'script',
            date: 1678692844546,
        },
    },
    {
        Q: 'ROS is the abbreviation of ...',
        A: 'Robot Operating System .',
        data: {
            type: 'simple',
            source: 'script',
            date: 1678692844546,
        },
    },
    {
        Q: 'A robot is ...',
        A: '... a machine—especially one programmable by a computer— capable of carrying out a complex series of actions automatically. Robots can be guided by an external control device or the control may be embedded within. Robots may be constructed on the lines of human form, but most robots are machines designed to perform a task with no regard to their aesthetics. .',
        data: {
            type: 'simple',
            source: 'script',
            date: 1678692844547,
        },
    },
    {
        Q: 'Complete the definition of a robot: A robot is a complex mechatronic system enabled with electronics, üres , actuators and software, executing tasks with a certain degree of üres . It may be preprogrammed, üres or carrying out computations to make üres .',
        A: 'Complete the definition of a robot: A robot is a complex mechatronic system enabled with electronics, [sensors], actuators and software, executing tasks with a certain degree of [autonomy]. It may be preprogrammed, [teleoperated] or carrying out computations to make [decisions].',
        data: {
            type: 'simple',
            source: 'script',
            date: 1678692844547,
        },
    },
]
export const questions = rawQuestions.map((q) => createQuestion(q))

export const subjects = [
    {
        Name: 'Pénzügyek alapjai',
        Questions: questions.slice(0, 4),
    },
    {
        Name: 'Programming robots in ROS',
        Questions: questions.slice(4, 8),
    },
    {
        Name: 'Programming something',
        Questions: questions.slice(8, 12),
    },
]

export const emptyQdb: QuestionDb = {
    index: 0,
    data: [],
    path: '',
    name: '',
    shouldSearch: '',
    shouldSave: {},
}
