import { getAvailableQdbIndexes } from '../utils/qdbUtils'
import { emptyQdb } from './testData'

test('getAvailableQdbIndexes works, normal order', () => {
    const qdbs = [0, 1, 2, 3, 4].map((x) => {
        return { ...emptyQdb, index: x }
    })
    const [index] = getAvailableQdbIndexes(qdbs)

    expect(index).toBe(5)
})

test('getAvailableQdbIndexes works, one missing', () => {
    const qdbs = [0, 1, 2, 3, 5].map((x) => {
        return { ...emptyQdb, index: x }
    })
    const [index] = getAvailableQdbIndexes(qdbs)

    expect(index).toBe(4)
})

test('getAvailableQdbIndexes works, empty qdb', () => {
    const [index] = getAvailableQdbIndexes([])

    expect(index).toBe(0)
})

test('getAvailableQdbIndexes works, multiple count', () => {
    const qdbs = [0, 1, 2, 3, 5].map((x) => {
        return { ...emptyQdb, index: x }
    })
    let indexes = getAvailableQdbIndexes(qdbs)
    expect(indexes.length).toBe(1)
    expect(indexes[0]).toBe(4)

    indexes = getAvailableQdbIndexes(qdbs, 5)
    expect(indexes).toStrictEqual([4, 6, 7, 8, 9])
})
