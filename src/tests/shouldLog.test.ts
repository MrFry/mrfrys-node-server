import { shouldLog } from '../utils/logger'

const noLogIds = ['1', '2', '10', '40']

const truthy = [1, 2, '10', '40']
const falsey = [5, '55', 47832, 'fhs']

test('ShouldLog works', () => {
    truthy.forEach((x) => {
        expect(shouldLog(x, noLogIds)).toBeFalsy()
    })
    falsey.forEach((x) => {
        expect(shouldLog(x, noLogIds)).toBeTruthy()
    })
})
